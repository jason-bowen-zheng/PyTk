# PyTk/widget.py

import tkinter as tk
import tkinter.ttk as ttk


class Window(tk.Tk):

    def __init__(self, title="PyTk", screenName=None, baseName=None, className="tk", useTk=1, sync=0, use=None):
        tk.Tk.__init__(self, screenName, baseName, className, useTk, sync, use)
        self.title(title)


class GameWindow(tk.Tk):

    def __init__(self, title="PyTk", screenName=None, baseName=None, className="tk", useTk=1, sync=0, use=None):
        tk.Tk.__init__(self, screenName, baseName, className, useTk, sync, use)
        self.title(title)
        self.canvas = tk.Canvas(self)
        self.canvas.grid(column=0, row=0, sticky="news")

    def get_canvas(self):
        return self.canvas
