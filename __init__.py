import PyTk.clipboard as clipboard
from   PyTk.const     import *
import PyTk.db        as db
import PyTk.widget    as widget
import PyTk.web       as web

import tkinter        as tk

PyTkVersion = 1.0
TkVersion = tk.TkVersion
TclVersion = tk.TclVersion
